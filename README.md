* Dissenter: https://dissenter.com/
* Freenet: https://freenetproject.org/
* Matrix: https://matrix.org/
* Mastodon: https://mastodon.social/about
* Tox: https://tox.chat/
* ZeroNet: https://zeronet.io/
* Jami: https://jami.net/